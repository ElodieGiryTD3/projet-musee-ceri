package fr.uavignon.ceri.tp3;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import androidx.fragment.app.FragmentResultListener;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import fr.uavignon.ceri.tp3.data.Item;

public class ListFragment extends Fragment {

    public static final String TAG = ListFragment.class.getSimpleName();

    private ListViewModel viewModel;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerAdapter adapter;

    private ProgressBar progress;

    Button retour;
    String resultItem;
    String resultCategorie;
    String searchItem;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getParentFragmentManager().setFragmentResultListener("resultAccueil", this, new FragmentResultListener() {

            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle bundle) {
                // We use a String here, but any type that can be put in a Bundle is supported
                resultItem = bundle.getString("spinnerItem");
                resultCategorie = bundle.getString("spinnerCategorie");
                searchItem = bundle.getString("searchItem");
                // Do something with the result
                Log.d(TAG,"LISTE spinner tri =  "+resultItem);
                Log.d(TAG,"LISTE spinner categorie = "+resultCategorie);
                Log.d(TAG,"LISTE search = "+searchItem);
            }
        });
    }



    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {

        View view = inflater.inflate(R.layout.fragment_list, container, false);

        retour = view.findViewById(R.id.buttonBackListItemToAccueil);
        retour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.action_ListFragment_to_AccueilFragment);
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        Log.d(TAG,"DANS ON VIEW CREATED DE LIST FRAGMENT");
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(ListViewModel.class);

        listenerSetup();
        observerSetup(); // enregistrement du fragment comme observateur du ViewModel

        //recyclerSetup();
    }

    private void listenerSetup() {
        recyclerView = getView().findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        //EG Ajout du paramètre dans le constructeur
        adapter = new RecyclerAdapter(R.layout.fragment_list);
        recyclerView.setAdapter(adapter);

        adapter.setListViewModel(viewModel);
        progress = getView().findViewById(R.id.progressList);

        /*
        FloatingActionButton fab = getView().findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ListFragmentDirections.ActionListFragmentToNewItemFragment action = ListFragmentDirections.actionListFragmentToNewItemFragment();
                action.setItemNum(Item.ADD_ID);
                Navigation.findNavController(view).navigate(action);

            }
        });

         */
    }

    //EG pour mise à jour des items à afficher
    private void observerSetup() {

        viewModel.getAllItemsByName().observe(getViewLifecycleOwner(),
                items -> adapter.setItemList(items));

        /*
        if(resultItem.equals("Nom")){
            Log.d("TAG", "TRI NOM");

        }
        else if(resultItem.equals("Date")){
            Log.d("TAG", "TRI DATE");
            //viewModel.getAllItemsByDate().observe(getViewLifecycleOwner(),
            //items -> adapter.setItemList(items));
        }

         */
    }

    //EG récupéré du modèle Room Demo
    private void recyclerSetup() {
        RecyclerView recyclerView;
        adapter = new RecyclerAdapter(R.layout.fragment_list);
        recyclerView = getView().findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
    }
}