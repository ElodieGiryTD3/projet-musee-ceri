package fr.uavignon.ceri.tp3;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;

import java.util.List;

import fr.uavignon.ceri.tp3.R;

public class AccueilFragment extends Fragment {
    Button catalogue;
    Button recherche;
    Spinner spinnerItem;
    Spinner spinnerCategorie;
    SearchView searchItem;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.accueil, container, false);

        catalogue = view.findViewById(R.id.catalogue);
        catalogue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.action_AccueilFragment_to_ListFragment);
            }
        });

        catalogue = view.findViewById(R.id.catalogue);
        catalogue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle resultAccueil = new Bundle();
                resultAccueil.putString("spinnerItem", spinnerItem.getSelectedItem().toString());
                resultAccueil.putString("spinnerCategorie",spinnerCategorie.getSelectedItem().toString());
                resultAccueil.putString("searchItem", searchItem.getQuery().toString());

                getParentFragmentManager().setFragmentResult("resultAccueil", resultAccueil);
                Navigation.findNavController(view).navigate(R.id.action_AccueilFragment_to_ListFragment);
            }
        });

        spinnerItem = view.findViewById(R.id.spinnerItem);
        Log.d("TAG","tri du spinner = "+spinnerItem.getSelectedItem().toString());

        spinnerCategorie = view.findViewById(R.id.spinnerCategorie);
        Log.d("TAG","categorie du spinner = "+spinnerCategorie.getSelectedItem().toString());

        searchItem = view.findViewById(R.id.search);
        Log.d("TAG","search = "+searchItem.getQuery().toString());

        return view;
    }

}