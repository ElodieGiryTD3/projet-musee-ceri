package fr.uavignon.ceri.tp3;


import android.app.Activity;
import android.content.Context;
import android.media.Image;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import fr.uavignon.ceri.tp3.data.Item;
import fr.uavignon.ceri.tp3.data.ItemRepository;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private static final String TAG = RecyclerAdapter.class.getSimpleName();
    private int itemItemLayout;
    private List<Item> itemList;
    private ItemRepository repository;
    private ListViewModel listViewModel;

    public RecyclerAdapter(int itemID) {
        itemItemLayout = itemID;
    }

    public void setItemList(List<Item> items) {
        itemList = items;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_layout, viewGroup, false);
        return new ViewHolder(v);
    }

    public static String startHttpRequest(String urlString){

        StringBuilder stringBuilder = new StringBuilder();

        try {
            // 1. Declare a URL Connection
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            // 2. Open InputStream to connection
            conn.connect();
            InputStream in = conn.getInputStream();
            // 3. Download and decode the string response using builder
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
        }
        catch (MalformedURLException exception){
        }
        catch (IOException exception) {
        }
        catch (Exception e){
        }

        return stringBuilder.toString();
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Log.d(TAG, "i = "+i);

        //EG : Affichage dans la liste du premier écran
        viewHolder.itemName.setText(itemList.get(i).getName());

        //Affichage alterné des items (image et texte alterné)
        if (i%2 == 0 ) {
            viewHolder.itemYear.setText(String.valueOf(itemList.get(i).getTimeFrame()));

            String uri = itemList.get(i).getItemID();
            String url ="https://demo-lia.univ-avignon.fr/cerimuseum/items/"+uri+"/thumbnail";
            Log.d(TAG, "url fabriquée DANS RECYCLER = "+url);

            // appel de la procédure faisant le download de l'image en asynchrone
            new DownloadImageTask((ImageView) viewHolder.itemIcon)
                    .execute(url);

            viewHolder.itemIcon1.setVisibility(View.INVISIBLE);

            if(itemList.get(i).getBrand().equals("")){
                viewHolder.itemBrand.setText("Marque inconnue");
            }
            else{
                viewHolder.itemBrand.setText(itemList.get(i).getBrand());
            }
            viewHolder.itemCategories.setText(itemList.get(i).getCategories());

            viewHolder.itemLike.setText(itemList.get(i).getLike()+" j'aimes");
        }
        else {

            viewHolder.itemYear1.setText(String.valueOf(itemList.get(i).getTimeFrame()));

            String uri = itemList.get(i).getItemID();
            String url ="https://demo-lia.univ-avignon.fr/cerimuseum/items/"+uri+"/thumbnail";
            Log.d(TAG, "url fabriquée DANS RECYCLER = "+url);

            // appel de la procédure faisant le download de l'image en asynchrone
            new DownloadImageTask((ImageView) viewHolder.itemIcon1)
                    .execute(url);

            viewHolder.itemIcon.setVisibility(View.INVISIBLE);

            if(itemList.get(i).getBrand().equals("")){
                viewHolder.itemBrand1.setText("Marque inconnue");
            }
            else{
                viewHolder.itemBrand1.setText(itemList.get(i).getBrand());
            }
            viewHolder.itemCategories1.setText(itemList.get(i).getCategories());

            viewHolder.itemLike1.setText(itemList.get(i).getLike()+" j'aimes");
        }

        //viewHolder.itemIcon.setImageURI(String.valueOf(itemList.get(i).getImage()));
    }

    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }

    public void setListViewModel(ListViewModel viewModel) {
        listViewModel = viewModel;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemName;
        TextView itemYear,itemYear1;
        TextView itemBrand,itemBrand1;
        TextView itemCategories,itemCategories1;
        TextView itemLike,itemLike1;
        ImageView itemIcon, itemIcon1;

        ActionMode actionMode;
        long idSelectedLongClick;

        ViewHolder(View itemView) {
            super(itemView);
            itemName = itemView.findViewById(R.id.item_name);
            itemYear = itemView.findViewById(R.id.item_year);
            itemYear1 = itemView.findViewById(R.id.item_year1);
            itemBrand = itemView.findViewById(R.id.item_brand);
            itemBrand1 = itemView.findViewById(R.id.item_brand1);
            itemCategories = itemView.findViewById(R.id.item_categories);
            itemCategories1 = itemView.findViewById(R.id.item_categories1);
            itemLike = itemView.findViewById(R.id.item_like);
            itemLike1 = itemView.findViewById(R.id.item_like1);
            itemIcon = itemView.findViewById(R.id.item_icon);
            itemIcon1 = itemView.findViewById(R.id.item_icon1);

            ActionMode.Callback actionModeCallback = new ActionMode.Callback() {

                // Called when the action mode is created; startActionMode() was called
                @Override
                public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                    // Inflate a menu resource providing context menu items
                    MenuInflater inflater = mode.getMenuInflater();
                    inflater.inflate(R.menu.context_menu, menu);
                    return true;
                }

                // Called each time the action mode is shown. Always called after onCreateActionMode, but
                // may be called multiple times if the mode is invalidated.
                @Override
                public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                    return false; // Return false if nothing is done
                }

                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    return false;
                }

                // Called when the user selects a contextual menu item
                /*
                @Override
                public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.menu_update:
                            ListFragmentDirections.ActionListFragmentToNewItemFragment action = ListFragmentDirections.actionListFragmentToNewItemFragment();
                            action.setItemNum(idSelectedLongClick);
                            Navigation.findNavController(itemView).navigate(action);
                            return true;
                        default:
                            return false;
                    }
                }

                 */

                // Called when the user exits the action mode
                @Override
                public void onDestroyActionMode(ActionMode mode) {
                    actionMode = null;
                }
            };

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    Log.d(TAG,"position="+getAdapterPosition());
                    long id = RecyclerAdapter.this.itemList.get((int)getAdapterPosition()).getId();
                    Log.d(TAG,"id="+id);

                    ListFragmentDirections.ActionListFragmentToDetailFragment action = ListFragmentDirections.actionListFragmentToDetailFragment();
                    action.setItemNum(id);
                    Navigation.findNavController(v).navigate(action);

                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {
                    idSelectedLongClick = RecyclerAdapter.this.itemList.get((int)getAdapterPosition()).getId();
                    if (actionMode != null) {
                        return false;
                    }
                    Context context = v.getContext();
                    // Start the CAB using the ActionMode.Callback defined above
                    actionMode = ((Activity)context).startActionMode(actionModeCallback);
                    v.setSelected(true);
                    return true;
                }
            });
        }
    }
}
