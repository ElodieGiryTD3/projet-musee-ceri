package fr.uavignon.ceri.tp3.data;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "item_table", indices = {@Index(value = {"itemID", "name"},
        unique = true)})
public class Item {

    public static final String TAG = Item.class.getSimpleName();

    public static final long ADD_ID = -1;

    @NonNull
    @ColumnInfo(name="itemID")
    private String itemID;

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name="_id")
    private long id;

    @NonNull
    @ColumnInfo(name="pictures")
    private String pictures;

    @NonNull
    @ColumnInfo(name="technicalDetails")
    private String technicalDetails;

    @NonNull
    @ColumnInfo(name="categories")
    private String categories;

    @NonNull
    @ColumnInfo(name="name")
    private String name;

    @NonNull
    @ColumnInfo(name="timeFrame")
    private String timeFrame;

    @NonNull
    @ColumnInfo(name="description")
    private String description;

    @NonNull
    @ColumnInfo(name="working")
    private boolean working;

    @NonNull
    @ColumnInfo(name="brand")
    private String brand;

    @NonNull
    @ColumnInfo(name="year")
    private Integer year;

    public Item(@NonNull String itemID,
                @NonNull String pictures,
                @NonNull String technicalDetails,
                @NonNull String categories,
                @NonNull String name,
                @NonNull String timeFrame,
                @NonNull String description,
                @NonNull boolean working,
                @NonNull String brand,
                @NonNull Integer year) {
        this.itemID = itemID;
        this.pictures = pictures;
        this.technicalDetails = technicalDetails;
        this.categories = categories;
        this.name = name;
        this.timeFrame = timeFrame;
        this.description=description;
        this.working = working;
        this.brand = brand;
        this.year=year;
    }

    public long getId() {
        return id;
    }

    public String getItemID() {return itemID;  }

    /*
    public List<Pictures> getPictures() {
        return pictures;
    }
    public List<TechnicalDetails> getTechnicalDetails1() {
        return technicalDetails1;
    }
     */

    public String getImgUri() {
        Log.d(TAG,"@drawable/img_"+itemID);
        return "@drawable/img_"+itemID;
    }

    public String getPictures() {
        return pictures;
    }

    public String getTechnicalDetails() {
        return technicalDetails;
    }
    /*
    public List<Categories> getCategories() {
        return categories;
    }

     */

    public String getCategories() {return categories;  }


    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }
    public boolean getWorking() {
        return working;
    }
    public String getBrand() {
        return brand;
    }

    public Integer getYear() {
        return year;
    }
    public String getTimeFrame() {
        return timeFrame;
    }


    public Integer getLike() {
        int tmp=10;
        return tmp;
    }

    public void setItemId(String itemID) { this.itemID= itemID;  }
    public void setId(long _id) { this.id= _id;  }
    @Override
    public String toString() {
        String temp = this.itemID + " " + this.name + " " +this.description ;
        return temp;
    }
}
