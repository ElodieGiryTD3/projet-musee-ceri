package fr.uavignon.ceri.tp3.data.webservice;

import java.util.Map;

import fr.uavignon.ceri.tp3.data.ItemResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

// EG : ajout de l'interface de l'API Musée du CERI

public interface ItemMuseeInterface {

    @Headers("Accept: application/geo+json")
    @GET("/cerimuseum/collection")
    Call <Map< String , ItemResponse>> getCollection();
}
