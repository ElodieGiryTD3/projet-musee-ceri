package fr.uavignon.ceri.tp3.data.database;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fr.uavignon.ceri.tp3.data.Item;
import fr.uavignon.ceri.tp3.data.ItemRepository;
import fr.uavignon.ceri.tp3.data.ItemResponse;

@Database(entities = {Item.class}, version = 5, exportSchema = false)
public abstract class ItemRoomDatabase extends RoomDatabase {

    private static final String TAG = ItemRoomDatabase.class.getSimpleName();

    public abstract ItemDao itemDao();

    private static ItemRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 1;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static ItemRoomDatabase getDatabase(final Context context) {
        Log.d(TAG,"INSTANCE ="+INSTANCE);
        if (INSTANCE == null) {
            synchronized (ItemRoomDatabase.class) {
                Log.d(TAG,"INSTANCE ="+INSTANCE);
                if (INSTANCE == null) {
                    // Create database here
                    // without populate


                    // with populate
                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    ItemRoomDatabase.class,"item_database")
                                    .addCallback(sRoomDatabaseCallback)
                                    .fallbackToDestructiveMigration()
                                    .build();


                }
            }
        }
        return INSTANCE;
    }

    private static Callback sRoomDatabaseCallback =
            new Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);
                    databaseWriteExecutor.execute(() -> {
                        // Populate the database in the background.
                        ItemDao dao = INSTANCE.itemDao();
                        dao.deleteAll();

                        Item[] items = {
                                new Item(
                                        "2j7",
                                        "",
                                        "CPU 8 bits (Motorola 6803)",
                                        "ordinateur de bureau",
                                        "Alice",
                                        "1980",
                                        "Ordinateur personnel français, clone du TRS-80 MC-10 de Tandy.",
                                        false,
                                        "Matra-Hachette",
                                        1983),

                                new Item(
                                        "ace",
                                        "IMG_1702",
                                        "40 à 80 Go par cartouche",
                                        "support de stockage",
                                        "Système de sauvegarde sur bande",
                                        "1999",
                                        "Cartouches à bande magnétique, utilisées principalement pour la sauvegarde de données de serveurs.",
                                        true,
                                        "Hewlett-Packard",
                                        1999),

                                new Item(
                                        "ci6",
                                        "IMG_1627",
                                        "Pour disquettes 5¼ pouces",
                                        "périphérique",
                                        "Lecteur de disquettes pour TO7",
                                        "1980",
                                        "Lecteur de disquettes accompagné du contrôleur dédié.",
                                        false,
                                        "Thomson",
                                        1984),

                                new Item(
                                        "blm",
                                        "IMG_1641",
                                        "Capacité de 16 Kio",
                                        "support de stockage",
                                        "Cartouche Mémo7",
                                        "1980",
                                        "Cartouche ROM utilisée pour la distribution de logiciels commerciaux pour ordinateurs Thomson (ici, pour TO7). Cet exemplaire contient un logiciel compatible Vidéotex pour accès au service Télétel (Minitel).",
                                        false,
                                        "",
                                        1984)
                        };

                        for(Item newItem : items)
                            dao.insert(newItem);
                        Log.d(TAG,"database populated");
                    });
                }
            };
}
