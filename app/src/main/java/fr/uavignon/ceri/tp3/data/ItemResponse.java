package fr.uavignon.ceri.tp3.data;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//EG : Ajout de la classe description de réponses de l'API
public class ItemResponse {

    //Attribut Musée

    //public String itemID=null;
    public Map<String, String> pictures=null;
    public List<String> technicalDetails = null;
    public List<String> categories = null;
    public String name=null;
    public int[] timeFrame=null;
    public String description=null;
    public boolean working=false;
    public String brand=null;
    public Integer year=0;

    public ItemResponse(Map<String, String> pictures,
                        List<String> technicalDetails,
                        List<String> categories,
                        String name,
                        int[] timeFrame,
                        String description,
                        boolean working,
                        String brand,
                        Integer year) {
        //this.itemID = itemID;
        this.pictures = pictures;
        this.technicalDetails = technicalDetails;
        this.categories = categories;
        this.name = name;
        this.timeFrame = timeFrame;
        this.description=description;
        this.working = working;
        this.brand = brand;
        this.year=year;
    }
}
