package fr.uavignon.ceri.tp3;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.bumptech.glide.RequestManager;

//import butterknife.BindView;
//import butterknife.ButterKnife;

import java.util.ArrayList;
import java.util.List;

import fr.uavignon.ceri.tp3.data.Item;

public class DetailFragment extends Fragment {
    public static final String TAG = DetailFragment.class.getSimpleName();

    private DetailViewModel viewModel;
    private TextView name, categorie, year, description, technicalDetail, timeFrame, brand, working, like;
    private TextView textItemId;
    private TextView labelLigne1, textLigne1, labelLigne2, textLigne2, labelLigne3, textLigne3, labelLigne4, textLigne4;
    private TextView labelLigne5, textLigne5, labelLigne6, textLigne6, labelLigne7, textLigne7, labelLigne8, textLigne8, labelLigne9, textLigne9, labelListeImage,textImage;
    private ImageView icone;
    private TextView listeImage;
    //private ProgressBar progress;

    private RequestManager glide;
    private List<Item> itemList;

    /*
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detail, container, false);
        //ButterKnife.bind(this, view);
        //this.configureRecyclerView();
        //this.configureSwipeRefreshLayout();
        //this.executeHttpRequestWithRetrofit();
        return view;
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);

        // Get selected item
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        long id = args.getItemNum();
        Log.d(TAG,"selected id="+id);
        viewModel.setItem(id);

        listenerSetup();
        observerSetup();
    }

    private void listenerSetup() {
        textItemId = getView().findViewById(R.id.textItemId);
        icone = getView().findViewById(R.id.icone);
        textLigne1= getView().findViewById(R.id.textLigne1);
        textLigne2= getView().findViewById(R.id.textLigne2);
        textLigne3= getView().findViewById(R.id.textLigne3);
        textLigne4= getView().findViewById(R.id.textLigne4);
        textLigne5= getView().findViewById(R.id.textLigne5);
        textLigne6= getView().findViewById(R.id.textLigne6);
        textLigne7= getView().findViewById(R.id.textLigne7);
        textLigne8= getView().findViewById(R.id.textLigne8);
        textLigne9= getView().findViewById(R.id.textLigne9);
        listeImage = getView().findViewById(R.id.textListeImage);


        getView().findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_DetailFragment_to_ListFragment);
            }
        });
    }

    private void observerSetup() {
        viewModel.getItem().observe(getViewLifecycleOwner(),
                item -> {
                    //textLigne5.setText("toto");
                    Log.d(TAG, "Avant test item null");

                    //technicalDetail.setText("titi");

                    if (item != null) {
                        Log.d(TAG, "observing item view");

                        Log.d(TAG, "item.getName() = "+item.getName());

                        Log.d(TAG, "item.getTechnicalDetails() = "+item.getTechnicalDetails());


                        textItemId.setText(item.getName());

                        String url ="https://demo-lia.univ-avignon.fr/cerimuseum/items/"+item.getItemID()+"/thumbnail";
                        Log.d(TAG, "url fabriquée = "+url);

                        // appel de la procédure faisant le download de l'image en asynchrone
                        //url="https://demo-lia.univ-avignon.fr/cerimuseum/items/ci6/images/IMG_1627";
                        new DownloadImageTask((ImageView) icone)
                                .execute(url);

                        //glide.load(url).into(icone);

/*
                        String uri = item.getImgUri();
                        Context c = icone.getContext();

                        try{
                            icone.setImageDrawable(c.getResources().getDrawable(
                                    c.getResources(). getIdentifier (uri , null , c.getPackageName())));
                        }
                        catch (Resources.NotFoundException e) {
                            uri = "@drawable/img_no_img";
                            c = icone.getContext();
                            icone.setImageDrawable(c.getResources().getDrawable(
                                    c.getResources(). getIdentifier (uri , null , c.getPackageName())));
                        }

 */


                        textLigne1.setText(item.getItemID());
                        textLigne2.setText(item.getCategories());

                        String tmpListePicture=item.getPictures();

                        List<String> listeCodeImage = new ArrayList<String>();
                        List<String> listeLabelImage = new ArrayList<String>();

                        if(tmpListePicture!=null){
                            Log.d(TAG, "CLES LUES = tmpListePicture =  "+tmpListePicture);
                            int debLabel=0;
                            int finLabel=0;
                            int debCodeImage=0;
                            String codeImage;
                            String labelImage;
                            debLabel=tmpListePicture.indexOf("|{",debLabel);
                            while(debLabel!=-1){
                                codeImage=tmpListePicture.substring(debCodeImage,debLabel);
                                listeCodeImage.add(codeImage);

                                url="https://demo-lia.univ-avignon.fr/cerimuseum/items/"+item.getItemID()+"/images/"+codeImage;
                                Log.d(TAG,"url codeImage = "+url);

                                finLabel=tmpListePicture.indexOf("}|",debLabel);
                                labelImage=tmpListePicture.substring(debLabel+2,finLabel);
                                listeLabelImage.add(labelImage);
                                debCodeImage=finLabel+2;
                                debLabel=tmpListePicture.indexOf("|{",finLabel);
                            }

                            Log.d(TAG, "CLES LUES = listeCodeImage = "+listeCodeImage);
                            Log.d(TAG, "CLES LUES = listeLabelImage = "+listeLabelImage);

                            if(listeCodeImage.size()>0){
                                //listeImage.setText(listeCodeImage.get(0) + "\n" + listeLabelImage.get(0));
                                url="https://demo-lia.univ-avignon.fr/cerimuseum/items/blm/images/"+listeCodeImage.get(0);
                                Log.d(TAG,"url picture = "+url);

                                listeImage.setText(listeCodeImage.get(0));
                            }
                        }

                        textLigne3.setText(String.valueOf(item.getYear()));
                        textLigne4.setText(String.valueOf(item.getTimeFrame()));

                        textLigne5.setText(item.getDescription());
                        textLigne6.setText(item.getTechnicalDetails());
                        textLigne7.setText(item.getBrand());
                        textLigne8.setText(String.valueOf(item.getWorking()));
                        textLigne9.setText(String.valueOf(item.getLike()));

                        if(item.getWorking()==true){
                            textLigne8.setText("Oui");
                        }
                        else{
                            textLigne8.setText("Non");
                        }
                    }
                });
    }
}