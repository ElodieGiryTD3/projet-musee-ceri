package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import fr.uavignon.ceri.tp3.data.Item;
import fr.uavignon.ceri.tp3.data.ItemRepository;

public class ListViewModel extends AndroidViewModel {
    private ItemRepository repository;
    private LiveData<List<Item>> allItems;

    /*
//EG ajout du constructeur pour utiliser le modèle de vue pour récupérer la liste d'item
    public ListViewModel (Application application) {
        super(application);
        repository = ItemRepository.get(application);
        allItems = repository.getAllItems();
    }

     */

    //EG ajout du constructeur pour utiliser le modèle de vue pour récupérer la liste d'item
    public ListViewModel (Application application) {
        super(application);
        repository = ItemRepository.get(application);
        allItems = repository.getAllItemsByName();

    }

    LiveData<List<Item>> getAllItems() {
        return allItems;
    }

    public LiveData<List<Item>> getAllItemsByName() {
        return allItems;
    }
}
