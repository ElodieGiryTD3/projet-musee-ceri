package fr.uavignon.ceri.tp3.data;

import android.app.Application;
import android.util.Log;
import android.widget.Spinner;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp3.ItemResult;
import fr.uavignon.ceri.tp3.R;
import fr.uavignon.ceri.tp3.data.database.ItemDao;

import fr.uavignon.ceri.tp3.data.database.ItemRoomDatabase;
import fr.uavignon.ceri.tp3.data.webservice.ItemMuseeInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

public class ItemRepository {

    private static final String TAG = ItemRepository.class.getSimpleName();

    private LiveData <List<Item>> allItems;
    private MutableLiveData<Item> selectedItem;

    private ItemDao itemDao;

    private static volatile ItemRepository INSTANCE;

    private static ItemRoomDatabase INSTANCE1;
    private static final int NUMBER_OF_THREADS = 1;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    //EG : déclaration de la variable de l'interface pour appeler le service web
    private final ItemMuseeInterface api;

    public synchronized static ItemRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new ItemRepository(application);
        }

        return INSTANCE;
    }
    //EG : déclaration de l'url du service web et de son API
    public ItemRepository(Application application) {
        Retrofit retrofit=
                new Retrofit.Builder()
                        .baseUrl("https://demo-lia.univ-avignon.fr")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();
        api = retrofit.create(ItemMuseeInterface.class);

        //Chargement des données à partir de la BDD
        //Pour le moment, chargées à partir de données statiques
        //A remplacer par le chargement des données à partir de l'API
        //
        //appel de getDatabase qui teste si la bdd existe,
        //si elle existe : on conserve les données
        //si elle n'existe pas : la charge à partir des données static
        ItemRoomDatabase db = ItemRoomDatabase.getDatabase(application);
        itemDao = db.itemDao();
        //charge toutes les données de la bdd, trié par date
        Log.d(TAG,"DANS ITEM REPOSITORY AVANT GET ALL ITEM BY NAME");


        //Spinner triPar = view.findViewById(R.id.catalogue);

        //Détermine l'ordre dans lequel les items récupérés dans la base sont affichés
        allItems = itemDao.getAllItemsByYear();

        Log.d(TAG,"allItems : "+allItems);
        selectedItem = new MutableLiveData<>();
    }

    public LiveData <List<Item>> getAllItemsByName() {
        Log.d(TAG,"DANS GET ALL ITEM");
        return allItems;
    }

    public MutableLiveData<Item> getSelectedItem() {
        return selectedItem;
    }

    public long insertItem(Item newItem) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return itemDao.insert(newItem);
        });
        long res = 0;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedItem.setValue(newItem);
        return res;
    }

    public int updateItem(Item item) {
        Future<Integer> fint = databaseWriteExecutor.submit(() -> {
            return itemDao.update(item);
        });
        int res = -1;
        try {
            res = fint.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedItem.setValue(item);
        return res;
    }

    public void getItem(long id)  {
        Future<Item> fitem = databaseWriteExecutor.submit(() -> {
            Log.d(TAG,"selected id="+id);
            return itemDao.getItemBy_Id(id);
        });
        try {
            selectedItem.setValue(fitem.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public LiveData<ItemResult> load() {
        Log.d(TAG,"DANS LOAD");
        final MutableLiveData<ItemResult> result = new MutableLiveData<>();

        result.setValue(new ItemResult(true, null, null));

        api.getCollection().enqueue(
                new Callback<Map<String,ItemResponse>>() {
                    @Override
                    public void onResponse(Call <Map<String, ItemResponse>> call,
                                           Response<Map<String, ItemResponse>> response) {

                        // Print keys and values
                        // L'API retourne les données dans le hashMap ItemResponse
                        Map<String, ItemResponse> map = new HashMap<String, ItemResponse>();
                        map = response.body();
                        Log.d(TAG,"CLES LUES = " + map.keySet().toString());

                        //On lit les valeurs de la hashMap response qui contient la liste des items retournés par l'API
                        //pour les insérer dans la base de données SQLite et les afficher dans la recyclerview
                        for (Map.Entry mapentry : map.entrySet()) {
                            Object valeur = mapentry.getValue();
                            Log.d(TAG," CLES LUES = -------------------------------------------------------");
                            ItemResponse itemResponse = map.get(mapentry.getKey());
                            String itemID = mapentry.getKey().toString();
                            Log.d(TAG,"CLES LUES = " + itemID);
                            Log.d(TAG,"CLES LUES = " + " --> Nom : " + itemResponse.name);
                            Log.d(TAG,"CLES LUES = " + " --> Description : " + itemResponse.description);

                            String picturesStr = "";
                            if (itemResponse.pictures != null) {
                                for (Map.Entry mapPictures : itemResponse.pictures.entrySet()) {
                                    String pictureID = mapPictures.getKey().toString();
                                    String pictureLabel = mapPictures.getValue().toString();
                                    picturesStr=picturesStr+pictureID+"|{"+pictureLabel+"}|";
                                }
                            }
                            Log.d(TAG, "CLES LUES = " + " ------> Pictures : " + picturesStr);

                            String technicalDetailsStr = "";
                            Log.d(TAG, "CLES LUES = " + " ------> technicalDetails = " + itemResponse.technicalDetails);
                            if (itemResponse.technicalDetails != null) {
                                technicalDetailsStr = itemResponse.technicalDetails.get(0);
                                for (int i = 1; i < itemResponse.technicalDetails.size(); i++) {
                                    technicalDetailsStr=technicalDetailsStr+"\n"+itemResponse.technicalDetails.get(i);
                                }
                            }
                            Log.d(TAG, "CLES LUES = " + " ------> technicalDetails : " + technicalDetailsStr);

                            String categoriesStr = "";
                            Log.d(TAG,"CLES LUES = " + " --> Catégorie : " + itemResponse.categories);
                            if (itemResponse.categories != null) {
                                categoriesStr = itemResponse.categories.get(0);
                                for (int i = 1; i < itemResponse.categories.size(); i++) {
                                    categoriesStr=categoriesStr+"\n"+itemResponse.categories.get(i);
                                }
                            }
                            Log.d(TAG, "CLES LUES = " + " ------> categories : " + categoriesStr);

                            String timeFrame = "";
                            if (itemResponse.timeFrame != null) {
                                timeFrame = String.valueOf(itemResponse.timeFrame[0]);
                                for (int i = 1; i < itemResponse.timeFrame.length; i++) {
                                    timeFrame = timeFrame + ", "+itemResponse.timeFrame[i];
                                }
                            }
                            Log.d(TAG, "CLES LUES = " + " --> timeFrame : "+timeFrame);

                            Log.d(TAG,"CLES LUES = " + " --> Working : " + itemResponse.working);
                            Log.d(TAG,"CLES LUES = " + " --> Brand : " + itemResponse.brand);
                            Log.d(TAG,"CLES LUES = " + " --> Year : " + itemResponse.year);

                            //On insère dans la base SQLite les données correspondant à l'item lu

                            Item item = new Item(itemID,
                                    picturesStr,
                                    technicalDetailsStr,
                                    categoriesStr,
                                    itemResponse.name,
                                    timeFrame,
                                    itemResponse.description,
                                    itemResponse.working,
                                    itemResponse.brand,
                                    itemResponse.year);

                            //itemDao.insert(item);

                            selectedItem.setValue(item);
                            ItemRoomDatabase.databaseWriteExecutor.execute(() -> {
                                itemDao.insert(item);
                            });
                        }
                    }

                    @Override
                    public void onFailure(Call<Map<String, ItemResponse>> call, Throwable t) {
                        Log.d("load", "failure: "+t.toString());
                        result.postValue(new ItemResult(false, null, t));
                    }
                });
        return result;
    }

}
