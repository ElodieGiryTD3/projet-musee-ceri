package fr.uavignon.ceri.tp3;

import android.app.Application;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;

import java.io.InputStream;
import java.util.ArrayList;

import fr.uavignon.ceri.tp3.data.Item;
import fr.uavignon.ceri.tp3.data.ItemRepository;
import fr.uavignon.ceri.tp3.data.ItemResponse;
import fr.uavignon.ceri.tp3.ItemResult;

public class DetailViewModel extends AndroidViewModel {
    final private ItemRepository repo = ItemRepository.get(getApplication());
    final MediatorLiveData<ItemResult> results = new MediatorLiveData<>();
    private LiveData<ItemResult> lastResult;

    public static final String TAG = DetailViewModel.class.getSimpleName();

    private ItemRepository itemRepository;
    private MutableLiveData<Item> item;

    public DetailViewModel (Application application) {
        super(application);
        itemRepository = ItemRepository.get(application);
        item = new MutableLiveData<>();
    }

    /*
    public void updateWithGithubUser(GithubUser githubUser, RequestManager glide){
        this.textView.setText(githubUser.getLogin());
        // 2 - Update TextView & ImageView
        this.texViewWebsite.setText(githubUser.getHtmlUrl());
        glide.load(githubUser.getAvatarUrl()).apply(RequestOptions.circleCropTransform()).into(imageView);
    }

     */

    public void setItem(long id) {
        itemRepository.getItem(id);
        item = itemRepository.getSelectedItem();
    }

    LiveData<Item> getItem() {
        return item;
    }
}