package fr.uavignon.ceri.tp3;

import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.Snackbar;

public class NewItemFragment extends Fragment {

    public static final String TAG = NewItemFragment.class.getSimpleName();

    private NewItemViewModel viewModel;
    private EditText editNewName, editNewCountry;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(NewItemViewModel.class);

        // Get selected item
        //NewItemFragmentArgs args = NewItemFragmentArgs.fromBundle(getArguments());
        //long itemID = args.getItemNum();
        //Log.d(TAG, "selected id=" + itemID);
        //viewModel.setItem(itemID);

        listenerSetup();
        observerSetup();
    }

    private void listenerSetup() {
        editNewName = getView().findViewById(R.id.editNewName);
        editNewCountry = getView().findViewById(R.id.editNewCountry);

        getView().findViewById(R.id.buttonInsert).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                Snackbar.make(view, "La ville a été ajoutée à la base de données",
                        Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }

        });

        /*
        getView().findViewById(R.id.buttonBack2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(NewItemFragment.this)
                        .navigate(R.id.action_NewItemFragment_to_ListFragment);
            }
        });

         */
    }

    private void observerSetup() {
        viewModel.getItem().observe(getViewLifecycleOwner(),
                item -> {
                    if (item != null) {
                        Log.d(TAG, "observing item view");
                        editNewName.setText(item.getName());
                    }
                });
    }
}
