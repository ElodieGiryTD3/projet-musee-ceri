package fr.uavignon.ceri.tp3.data.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import fr.uavignon.ceri.tp3.data.Item;

@Dao
public interface ItemDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Item item);

    @Query("DELETE FROM item_table")
    void deleteAll();

    @Query("DELETE FROM item_table WHERE itemID = :idItem")
    void deleteItem(String idItem);

    //Liste des Item par ordre alphabétique selon leurs noms
    @Query("SELECT * from item_table ORDER BY name ASC")
    LiveData <List<Item>> getAllItemsByName();

    //Liste des Item par ordre chronologique ascendant selon leurs dates
    @Query("SELECT * from item_table ORDER BY year ASC")
    LiveData <List<Item>> getAllItemsByYear();

    //Liste des Item par popularité
    @Query("SELECT * from item_table ORDER BY brand ASC")
    LiveData <List<Item>> getAllItemsByPopularite();


    @Query("SELECT * FROM item_table WHERE itemID = :idItem")
    Item getItemByItemId(String idItem);

    @Query("SELECT * FROM item_table WHERE _id = :id")
    Item getItemBy_Id(long id);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    int update(Item item);
}
