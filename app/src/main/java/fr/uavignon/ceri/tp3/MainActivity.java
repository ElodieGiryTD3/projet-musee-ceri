package fr.uavignon.ceri.tp3;

import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import android.util.Log;
import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

import fr.uavignon.ceri.tp3.data.Item;
import fr.uavignon.ceri.tp3.data.ItemRepository;
import fr.uavignon.ceri.tp3.data.database.ItemDao;
import fr.uavignon.ceri.tp3.data.database.ItemRoomDatabase;

public class MainActivity extends AppCompatActivity {
    private ItemDao itemDao;
    private LiveData<List<Item>> allItems;
    private MutableLiveData<Item> selectedItem;

    private ItemRepository repo = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        String[] triPar={"Nom","Date","Catégories","Popularité"};

        super.onCreate(savedInstanceState);

        //setContentView(R.layout.activity_main);
        //EG : appel de l'acceuil
        setContentView(R.layout.activity_main);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        repo = ItemRepository.get(getApplication());
        repo.load();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_update_all) {

            //EG : déclaration de la variable clé de l'API
            List<Item> listeItem;

            ItemRoomDatabase db =  ItemRoomDatabase.getDatabase(getApplication());
            itemDao = db.itemDao();
            allItems= itemDao.getAllItemsByName();

        }

        return super.onOptionsItemSelected(item);
    }
}