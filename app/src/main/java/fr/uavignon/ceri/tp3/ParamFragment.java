package fr.uavignon.ceri.tp3;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

public class ParamFragment extends Fragment {

    Button retour;
    /*
    String[] triItem={"Nom","Date","Popularité","Catégorie"};
    String[] selectCategorie={"règle à calcul",
            "ordinateur de poche",
            "réseau",
            "8 bits",
            "terminal de communication",
            "station de travail",
            "ordinateur",
            "ordinateur de bureau",
            "écran",
            "téléphone",
            "periphérique",
            "ordinateur portable",
            "câble/adaptateur",
            "SCSI",
            "support de stockage",
            "prototype",
            "périphérique",
            "composant"};

     */
    //int images[] = {R.drawable.apple,R.drawable.grapes, R.drawable.mango, R.drawable.pineapple, R.drawable.strawberry };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_param, container, false);

        Spinner spinnerItem = view.findViewById(R.id.spinnerItem);
        Spinner spinnerCategorie = view.findViewById(R.id.spinnerCategorie);

        retour = view.findViewById(R.id.buttonBackTriToAccueil);
        retour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.action_ParamFragment_to_AccueilFragment);
            }
        });

        /*
        spinnerItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(view).navigate(R.id.action_AccueilFragment_to_ListFragment);
            }
        });

         */

        return view;
    }
}